ARGUMENTS=(
	-m 4G
	-enable-kvm
	-net nic
	-net bridge,br=qemu_bridge
	# -kernel bootBackup/boot/vmlinuz-linux
	# -initrd bootBackup/boot/initramfs-linux.img
	# -kernel vmlinuz-linux
	# -initrd initramfs-linux.img
	# -drive file=sdapu,format=raw
	# -initrd /boot/initramfs-linux.img
	# -blockdev driver=raw,node-name=disk,file.driver=file,file.filename=/dev/sdb1
	# -append 'root=/dev/sda1  rootfstype=ext4 rw console=ttyS0' -nographic
	# -cdrom create_arch/archlinux-2017.02.01-x86_64.iso -boot order=d 
	# -append 'init=/bin/sh'
	# -append 'root=UUID=f3bfbe4b-6c87-4ee8-9226-b44b01a463b7'
	# -drive file=/dev/sdb1
	# -boot menu=on
	# -drive file=archDocker/archlive/work/rawhd_blockfile,format=raw
	# sdapu
	work/rawhd_blockfile
	# /dev/sdb
	)

qemu-system-x86_64 "${ARGUMENTS[@]}"
