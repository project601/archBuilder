#!/bin/bash

set -e -u

install_dir=arch
work_dir=work
out_dir=out
gpg_key=
linux_version=$(pacman -S linux --print-format "%v")-ARCH
arch=$(uname -m)
verbose=""
block_dev_file="rawhd_blockfile"
BUILD_ENVIRONMENT="PRODUCTION"

_usage ()
{
    echo "usage ${0} [options]"
    echo
    echo " General options:"
    echo "    -L <linux_version> Set a linux kernel version"
    echo "                        Default: ${linux_version}"
    echo "    -D <install_dir>   Set an install_dir (directory inside iso)"
    echo "                        Default: ${install_dir}"
    echo "    -t                 build for testing environment"
    echo "                        Default: Production"
    echo "    -w <work_dir>      Set the working directory"
    echo "                        Default: ${work_dir}"
    echo "    -o <out_dir>       Set the output directory"
    echo "                        Default: ${out_dir}"
    echo "    -v                 Enable verbose output"
    echo "    -h                 This help message"
    exit ${1}
}

_msg_info() {
    local _msg="${1}"
    echo "[mkarchhd] INFO: ${_msg}"
}

make_pacman_conf() {
    local _cache_dirs
    _cache_dirs=($(pacman -v 2>&1 | grep '^Cache Dirs:' | sed 's/Cache Dirs:\s*//g'))
    sed -r "s|^#?\\s*CacheDir.+|CacheDir = $(echo -n ${_cache_dirs[@]})|g" ./customizedFiles/pacman.conf > ${work_dir}/pacman.conf
}

make_packages() {
    mkdir -p ${work_dir}/airootfs
    pkg_list="$(echo $(grep -h -v ^# ./customizedFiles/packages.both))"
    pacstrap -C "${work_dir}/pacman.conf" -c -d -G -M \
	    "${work_dir}/airootfs" ${pkg_list}
}

make_setup_mkinitcpio() {
    cp ./customizedFiles/mkinitcpio.conf ${work_dir}/airootfs/etc/
    eval arch-chroot ${work_dir}/airootfs mkinitcpio -k /boot/vmlinuz-linux -g /boot/initramfs-linux.img
    
    # gnupg_fd=
    # if [[ ${gpg_key} ]]; then
    #   gpg --export ${gpg_key} >${work_dir}/gpgkey
    #   exec 17<>${work_dir}/gpgkey
    # fi
    # ARCHISO_GNUPG_FD=${gpg_key:+17} setarch ${arch} mkrawhd ${verbose} -w "${work_dir}/${arch}" -C "${work_dir}/pacman.conf" -D "${install_dir}" -r 'mkinitcpio -c /etc/mkinitcpio-archiso.conf -k /boot/vmlinuz-linux -g /boot/archiso.img' run
    # if [[ ${gpg_key} ]]; then
    #   exec 17<&-
    # fi
}

make_customize_airootfs() {
    cp -alf --no-preserve=ownership ./customizedFiles/airootfs ${work_dir}/

    # replace custom configuration variables 
    export $(grep -v '^#' ./environment.template | xargs)

    if [[ -e ./environment.private ]]; then
	    export $(grep -v '^#' ./environment.private | xargs)
    fi

    if [[ -e ./environment.testing && "${BUILD_ENVIRONMENT}" == "TESTING" ]]; then
	    export $(grep -v '^#' ./environment.testing | xargs)
    fi


    curl -o ${work_dir}/airootfs/etc/pacman.d/mirrorlist 'https://www.archlinux.org/mirrorlist/?country=all&protocol=http&use_mirror_status=on'

    conf_path=${work_dir}/airootfs/etc/hostname
    cat ${conf_path}.subst | envsubst '$HOSTNAME' > $conf_path

    conf_path=${work_dir}/airootfs/root/customize_airootfs.sh
    cat ${conf_path}.subst | envsubst '$USRNAME $PSW' > $conf_path
    chmod +x ${conf_path}

    eval BUILD_ENVIRONMENT=${BUILD_ENVIRONMENT} arch-chroot ${work_dir}/airootfs /root/customize_airootfs.sh
    rm ${work_dir}/airootfs/root/customize_airootfs.sh
}

_mount_rawhdfs() {
    trap "_umount_rawhdfs" EXIT HUP INT TERM
    mkdir -p "${work_dir}/mnt/rawhdfs"
    mount -o loop,offset=1048576 "${work_dir}/${block_dev_file}" "${work_dir}/mnt/rawhdfs"
}

_umount_rawhdfs() {
    umount -d "${work_dir}/mnt/rawhdfs"
    losetup --list
    rmdir "${work_dir}/mnt/rawhdfs"
    trap - EXIT HUP INT TERM
}

make_filetransfer() {
    cp -aT "${work_dir}/airootfs/" "${work_dir}/mnt/rawhdfs/"
    }

make_syslinux() {
    pacstrap -C "${work_dir}/pacman.conf" -c -d -G -M \
	    "${work_dir}/mnt/rawhdfs" syslinux
    cp -af ./customizedFiles/syslinux ${work_dir}/mnt/rawhdfs/boot/
    cp -af ${work_dir}/mnt/rawhdfs/usr/lib/syslinux/bios/*.c32 ${work_dir}/mnt/rawhdfs/boot/syslinux
    extlinux --install ${work_dir}/mnt/rawhdfs/boot/syslinux
    dd bs=440 count=1 conv=notrunc if=${work_dir}/mnt/rawhdfs/usr/lib/syslinux/bios/mbr.bin of="${work_dir}/${block_dev_file}"
}

if [[ ${EUID} -ne 0 ]]; then
    echo "This script must be run as root."
    _usage 1
fi

if [[ ${arch} != x86_64 ]]; then
    echo "This script needs to be run on x86_64"
    _usage 1
fi

while getopts 'N:V:L:D:w:o:g:tvh' arg; do
    case "${arg}" in
        L) linux_version="${OPTARG}" ;;
        D) install_dir="${OPTARG}" ;;
        w) work_dir="${OPTARG}" ;;
        o) out_dir="${OPTARG}" ;;
        g) gpg_key="${OPTARG}" ;;
	t) BUILD_ENVIRONMENT="TESTING";;
        v) verbose="-v" ;;
        h) _usage 0 ;;
        *)
           echo "Invalid argument '${arg}'"
           _usage 1
           ;;
    esac
done

# switch to path of this file you are reading right now
cd "$(readlink -f ${0%/*})"

mkdir -p ${work_dir}

_msg_info "Copy cache dir line of host pacman.conf to work/pacman.conf"
make_pacman_conf

_msg_info "Create '"${work_dir}/airootfs"' and pacstrap pkgs into it"
make_packages

_msg_info "Copy custom files and run scripts in '"${work_dir}/airootfs"'"
make_customize_airootfs

_msg_info "Running mkinitcpio in '"${work_dir}/airootfs"' chroot"
make_setup_mkinitcpio

_msg_info "Mounting '"${work_dir}/${block_dev_file}"' on '${work_dir}/mnt/rawhdfs'"
_mount_rawhdfs

_msg_info "Copying '${work_dir}/airootfs/' to '${work_dir}/mnt/rawhdfs/'..."
make_filetransfer

_msg_info "Installing syslinux on '${work_dir}/mnt/rawhdfs/boot/syslinux'..."
make_syslinux

_msg_info "Unmounting '${work_dir}/mnt/rawhdfs'"
_umount_rawhdfs

_msg_info "Done!"
